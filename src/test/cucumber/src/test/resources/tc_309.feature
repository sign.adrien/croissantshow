# language: fr
Fonctionnalité: Test Connexion passant
  
  Scénario:
    Etant donné La base de données contient des utilisateurs
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par "Admin"
    Et le champ "mot de passe" a été rempli par "adminadmin"
    Quand l'utilisateur soumet ses identifiants
    Alors l'utilisateur est connecté
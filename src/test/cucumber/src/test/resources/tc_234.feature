# language: fr
Fonctionnalité: Test Inscription Nom Manquant

  Scénario:
    Etant donné l'utilisateur est sur la page d'inscription
    Et l'utilisateur "jedu" est inconnu
    Et le champ d'inscription "nom d'utilisateur" a été rempli par "jedu"
    Et le champ "prénom" a été rempli par "Jean"
    Et le champ d'inscription "mot de passe" a été rempli par "12345678"
    Et le champ "confirmation mot de passe" a été rempli par "12345678"
    Et le champ "adresse e-mail" a été rempli par "jean.dupont@test.test"
    Quand l'utilisateur soumet le formulaire
    Alors un message d'erreur s'affiche indiquant que le nom doit être renseigné
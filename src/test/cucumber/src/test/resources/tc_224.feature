# language: fr
Fonctionnalité: Test Inscription Mdp Manquant
  
  Scénario:
    Etant donné l'utilisateur est sur la page d'inscription
    Et le champ d'inscription "nom d'utilisateur" a été rempli par "jedu"
    Et le champ "prénom" a été rempli par "Jean"
    Et le champ "nom" a été rempli par "Dupont"
    Et le champ "adresse e-mail" a été rempli par "jean.dupont@test.test"
    Et le champ "confirmation mot de passe" a été rempli par "12345678"
    Quand l'utilisateur soumet le formulaire
    Alors un message d'erreur s'affiche indiquant que le mot de passe doit être renseigné
# language: fr
Fonctionnalité: Test Connexion MdP Manquant
  
    Scénario:
    Etant donné La base de données contient des utilisateurs
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par "jedu"
    Et le champ "mot de passe" a été rempli par ""
    Quand l'utilisateur soumet ses identifiants
    Alors un message d'erreur s'affiche indiquant que le mot de passe n'a pas été renseigné
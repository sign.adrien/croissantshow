# language: fr
Fonctionnalité: Test Mail Evenement Annulé
  Etant donné La base de données contient un utilisateur ayant pour adresse e-mail "jean.dupont@test.test" et pour prénom "Jean" et pour nom "Dupont" et pour nom d'utilisateur "jedu"
  Et la base de données contient un utilisateur ayant pour adresse e-mail "tartare@test.test" et pour prénom "Tar" et pour nom "Tare" et pour nom d'utilisateur "tartare"
  Et l'utilisateur "jedu" et l'utilisateur "tartare" sont les seuls participants pour un mecredi donné
  Quand l'utilisateur "tartare" informe de son indisponibilité pour la prochaine séance
  Alors un e-mail est envoyé à "jean.dupont@test.test" l'informant que l'évenement est annulé
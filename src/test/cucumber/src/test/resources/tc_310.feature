# language: fr
Fonctionnalité: Test Assignation Croissants Deux Fois
  
  Scénario:
    Etant donné La base de données contient un utilisateur ayant pour nom d'utilisateur "jedu"
    Et l'utilisateur "jedu" était responsable des croissants à la dernière séance
    Alors l'utilisateur "jedu" ne peut pas être responsable des croissants à la prochaine séance
    
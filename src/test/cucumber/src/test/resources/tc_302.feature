# language: fr
Fonctionnalité: Test Mail Rappel
  
  Scénario:
    Etant donné La base de données contient un utilisateur ayant pour adresse e-mail "jean.dupont@test.test" et pour prénom "Jean" et pour nom "Dupont" et pour nom d'utilisateur "jedu"
    Et la base de données contient un utilisateur ayant pour adresse e-mail "tartare@test.test" et pour prénom "Tar" et pour nom "Tare" et pour nom d'utilisateur "tartare"
    Et la base de données contient un utilisateur ayant pour adresse e-mail "coco@test.test" et pour prénom "Co" et pour nom "Co" et pour nom d'utilisateur "coco"
    Et l'utilisateur "jedu" est le prochain responsable
    Quand nous sommes "lundi"
    Alors un e-mail de rappel est envoyé à l'adresse e-mail "jean.dupont@test.test" rappelant à "Jean Dupont" qu'il est le prochain responsable
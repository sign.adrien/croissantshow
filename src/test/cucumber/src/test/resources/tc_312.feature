# language: fr
Fonctionnalité: Test Mail Changement Responsables
  
  Scénario:
    Etant donné La base de données contient un utilisateur ayant pour adresse e-mail "jean.dupont@test.test" et pour prénom "Jean" et pour nom "Dupont" et pour nom d'utilisateur "jedu"
    Et la base de données contient un utilisateur ayant pour adresse e-mail "tartare@test.test" et pour prénom "Tar" et pour nom "Tare" et pour nom d'utilisateur "tartare"
    Et la base de données contient un utilisateur ayant pour adresse e-mail "coco@test.test" et pour prénom "Co" et pour nom "Co" et pour nom d'utilisateur "coco"
    Et l'utilisateur "jedu" est responsable dans deux séances
    Et l'utilisateur "tartare" est le prochain responsable
    Quand l'utilisateur "tartare" informe de son indisponibilité pour la prochaine séance
    Alors le prochain responsable change pour être l'utilisateur "jedu"
    Et un e-mail de changement est envoyé à l'adresse e-mail "jean.dupont@test.test" indiquant à "Jean Dupont" qu'il est le prochain responsable
    Et un e-mail de changement est envoyé à l'adresse e-mail "tartare@test.test" indiquant à "Tar Tare" la nouvelle date pour laquelle il sera responsable
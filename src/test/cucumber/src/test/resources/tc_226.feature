# language: fr
Fonctionnalité: Test inscription passant
  
  Scénario:
    Etant donné l'utilisateur est sur la page d'inscription
    Et le champ d'inscription "nom d'utilisateur" a été rempli par "jedu2"
    Et le champ "prénom" a été rempli par "Jean"
    Et le champ "nom" a été rempli par "Dupont"
    Et le champ d'inscription "mot de passe" a été rempli par "12345678"
    Et le champ "confirmation mot de passe" a été rempli par "12345678"
    Et le champ "adresse e-mail" a été rempli par "jean2.dupont@test.test"
    Quand l'utilisateur soumet le formulaire
    Alors un message de validation s'affiche
    Et les données entrées sont en attente de validation par l'administrateur pour l'utilisateur "jedu2"
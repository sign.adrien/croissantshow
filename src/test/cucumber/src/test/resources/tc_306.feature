# language: fr
Fonctionnalité: Test Connexion Utilisateur Manquant
  
  Scénario:
    Etant donné  La base de données contient des utilisateurs
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par ""
    Et le champ "mot de passe" a été rempli par "12345678"
    Quand l'utilisateur soumet ses identifiants
    Alors un message d'erreur s'affiche indiquant que l'identifiant doit être renseigné
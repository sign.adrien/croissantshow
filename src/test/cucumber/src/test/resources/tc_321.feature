# language: fr
Fonctionnalité: Nouveau participant

Scénario:
  Etant donné le Google Agenda déjà rempli
  Et l'utilisateur "jedu" s'inscrit à l'évènement
  Quand l'administrateur accepte "jedu"
  Alors le Google Agenda est mis à jour
  Et l'utilisateur "jedu" est ajouté en tête de la prochaine liste de responsables
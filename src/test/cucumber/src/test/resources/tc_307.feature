# language: fr
Fonctionnalité: Test Assignation Croissants Passant
  
  Scénario:
    Etant donné La base de données contient un utilisateur ayant pour nom d'utilisateur "jedu"
    Et l'utilisateur "jedu" n'a pas été responsable des croissants depuis autant de semaines qu'il y a de participants
    Alors l'utilisateur "jedu" peut être responsable des croissants
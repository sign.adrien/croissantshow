# language: fr
Fonctionnalité: Test mis à jour Agenda

 Scénario:
  Etant donné le Google Agenda déjà rempli
  Et l'utilisateur "jedu" est le prochain responsable
  Quand "jedu" se désiste
  Alors le Google Agenda est mis à jour
  Et les responsables se décalent d'une semaine
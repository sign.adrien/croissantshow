# language: fr
Fonctionnalité: Test Connexion MdP Incorrect
  
  Scénario:
    Etant donné La base de données contient des utilisateurs
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par "Admin"
    Et le champ "mot de passe" a été rempli par "WrongPassword"
    Quand l'utilisateur soumet ses identifiants
    Alors un message d'erreur s'affiche indiquant que les informations de connexions sont incorrects
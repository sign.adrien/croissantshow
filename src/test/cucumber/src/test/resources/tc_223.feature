# language: fr
Fonctionnalité: Test Inscription Mail Manquant
  
  Scénario:
    Etant donné l'utilisateur est sur la page d'inscription
    Et le champ d'inscription "nom d'utilisateur" a été rempli par "jedu"
    Et le champ "prénom" a été rempli par "Jean"
    Et le champ "nom" a été rempli par "Dupont"
    Et le champ d'inscription "mot de passe" a été rempli par "12345678"
    Et le champ "confirmation mot de passe" a été rempli par "12345678"
    Quand l'utilisateur soumet le formulaire
    Alors un message d'erreur s'affiche indiquant que l'adresse e-mail doit être renseigné
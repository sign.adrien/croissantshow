# language: fr
Fonctionnalité: Deconnexion

  Scénario: Deconnexion d'un administrateur
    Etant donné La base de données contient des utilisateurs
    Et le nom d'utilisateur "admin" est deja existant dans la base de données
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par "admin"
    Et le champ "mot de passe" a été rempli par "adminadmin"
    Quand l'utilisateur soumet ses identifiants
    Alors l'utilisateur est connecté
    Quand l'utilisateur appuie sur le bouton deconnexion
    Alors l'utilisateur est deconnecté

  Scénario: Deconnexion d'un utilisateur
    Etant donné La base de données contient des utilisateurs
    Et le nom d'utilisateur "loic" est deja existant dans la base de données
    Et l'utilisateur est sur la page de connexion
    Et le champ "nom d'utilisateur" a été rempli par "loic"
    Et le champ "mot de passe" a été rempli par "adminadmin"
    Quand l'utilisateur soumet ses identifiants
    Alors l'utilisateur est connecté
    Quand l'utilisateur appuie sur le bouton deconnexion
    Alors l'utilisateur est deconnecté
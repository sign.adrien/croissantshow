package acceptance;

import cucumber.api.CucumberOptions;
import cucumber.api.java.Before;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.sql.SQLException;


@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty",  "html:target/test-report",
        "json:target/test-report/croissantShowComplement.json",
        "junit:target/test-report/croissantShowComplement.xml",
},
        features = "src/test/resources2")
public class RunComplementTest {

}

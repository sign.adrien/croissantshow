package acceptance;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Etantdonné;
import cucumber.api.java.fr.Quand;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ConnexionFixture {
    private WebDriver driver = new HtmlUnitDriver(BrowserVersion.CHROME);
    private Connection connection;
    private String baseUrl;
    private DBQueries db;
    {
        try {
            db = new DBQueries();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp() throws SQLException, ClassNotFoundException {
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF);
        baseUrl = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test2/TF/";
        driver.get(baseUrl+ "php/login.php");
        String dbUrl= "jdbc:mysql://172.20.128.68/m2test2TF?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String dbUsername= "m2test2";
        String dbPassword= "m2test2";
        connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
    }

    @Quand("^l'utilisateur appuie sur le bouton deconnexion$")
    public void appuieBoutonDeconnexion() throws SQLException {
        driver.findElement(By.id("btn-deconnexion")).click();
    }

    @Quand("^l'utilisateur est deconnecté$")
    public void deconnexionUtilisateur() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
    }

    @Quand("^l'utilisateur \"([^\"]*)\" est inconnu$")
    public void deconnexionUtilisateur(String username) throws SQLException {
        if(db.existUser(username)) {
            db.supprimerUtilisateur(username);
        }
    }

    @Etantdonné("^La base de données contient des utilisateurs$")
    public void contientBDDUtilisateur() throws SQLException {
    }

    @Et("^l'utilisateur est sur la page de connexion$")
    public void estSurLaPageDeLogin() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
    }

    @Et("^le champ \"nom d'utilisateur\" a été rempli par \"([^\"]*)\"$")
    public void champNomUtilisateur(String nom) throws SQLException {
        driver.findElement(By.name("nomUser")).sendKeys(nom);
    }

    @Et("^le champ \"mot de passe\" a été rempli par \"([^\"]*)\"$")
    public void champMDP(String mdp) throws SQLException {
        driver.findElement(By.name("mdp")).sendKeys(mdp);
    }

    @Quand("^l'utilisateur soumet ses identifiants$")
    public void envoyerConnexion() throws SQLException {
        driver.findElement(By.id("btnConnect")).click();
    }

    @Alors("^un message d'erreur s'affiche indiquant que ses informations de connexion sont incorrectes$")
    public void messageerreur() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".form-grouphas-error > .help-block")).getText(), is("Le compte n\'existe pas."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que l'identifiant doit être renseigné$")
    public void messageerreur2() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".form-grouphas-error > .help-block")).getText(), is("Entrer votre nom de compte"));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le mot de passe n'a pas été renseigné$")
    public void messageerreur3() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".form-grouphas-error > .help-block")).getText(), is("Entrer votre mot de passe."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que les informations de connexions sont incorrects$")
    public void messageerreur4() throws SQLException {
        assertEquals(baseUrl + "php/login.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".form-grouphas-error > .help-block")).getText(), is("Le mot de passe saisi n'est pas valide."));
    }

    @Alors("^l'utilisateur est connecté$")
    public void messageerreur5() throws SQLException {
        assertEquals(baseUrl + "index.php", driver.getCurrentUrl());
    }
}
package acceptance;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.apache.commons.codec.digest.DigestUtils;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import sun.security.provider.MD5;
import org.apache.ibatis.jdbc.ScriptRunner;
import at.favre.lib.crypto.bcrypt.*;
import at.favre.lib.bytes.Bytes;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.sql.*;

public class DBQueries {

    private static Connection  connection;

    public DBQueries() throws SQLException {

        String dbUrl= "jdbc:mysql://172.20.128.68/m2test2TF?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String dbUsername= "m2test2";
        String dbPassword= "m2test2";

        connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
    }

    public void regenerateBDD() throws SQLException {
        /*String query = "DROP TABLE Participation;";
        Statement st = connection.createStatement();
        st.executeUpdate(query);

        query = "DROP TABLE Message;";
        st = connection.createStatement();
        st.executeUpdate(query);

        query = "DROP TABLE MercrediCroissant;";
        st = connection.createStatement();
        st.executeUpdate(query);

        query = "DROP TABLE User;";
        st = connection.createStatement();
        st.executeUpdate(query);*/

        String aSQLScriptFilePath = "src/insert.sql";
        try {
            // Initialize object for ScripRunner
            ScriptRunner sr = new ScriptRunner(connection);

            // Give the input file to Reader
            Reader reader = new BufferedReader(new FileReader(aSQLScriptFilePath));

            // Exctute script
            sr.runScript(reader);

        } catch (Exception e) {
            System.err.println("Failed to Execute" + aSQLScriptFilePath
                    + " The error is " + e.getMessage());
        }

    }

    public boolean existUser(String username) throws SQLException {
        String query = "SELECT * FROM `User` WHERE nomUser LIKE \"" + username + "\";";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (!rs.next()) {
            return false;
        }
        return true;
    }

    public void addUser(String username, String nom, String prenom, String mail, String mdp) throws SQLException {
            String myHash = BCrypt.withDefaults().hashToString(12, mdp.toCharArray());
            String query = "INSERT INTO User VALUES (0, '" + username + "', '" + prenom + "', '" + nom + "', '" + mail + "', '" + myHash + "', 0, 0, 0, 0);";
            Statement st = connection.createStatement();
            st.executeUpdate(query);
    }

    public boolean existEmail(String mail) throws SQLException  {
        String query = "SELECT * FROM `User` WHERE email LIKE \"" + mail + "\";";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        if (!rs.next()) {
            return false;
        }
        return true;
    }

    public boolean enAttente(String username) throws SQLException {
        String query = "SELECT * FROM `User` WHERE nomUser = \"" + username + "\";";
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        rs.next();
        if (rs.getInt("validInscription") == 0) {
            return true;
        }
        return false;
    }

    public void supprimerUtilisateur(String username) throws SQLException {
        String query = "DELETE FROM User WHERE nomUSER LIKE \"" + username + "\";";
        Statement st = connection.createStatement();
        st.executeUpdate(query);
    }
}
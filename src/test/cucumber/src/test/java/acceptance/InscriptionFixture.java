package acceptance;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import cucumber.api.java.Before;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Etantdonné;
import cucumber.api.java.fr.Quand;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class InscriptionFixture {
    private WebDriver driver = new HtmlUnitDriver(BrowserVersion.CHROME);
    private Connection connection;
    private String baseUrl;
    private DBQueries db;
    {
        try {
            db = new DBQueries();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp() throws SQLException, ClassNotFoundException {
        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF);
        baseUrl = "http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test2/TF/";
        driver.get(baseUrl+ "php/register.php");
        String dbUrl= "jdbc:mysql://172.20.128.68/m2test2TF?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String dbUsername= "m2test2";
        String dbPassword= "m2test2";

        connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
    }

    @Etantdonné("^l'utilisateur est sur la page d'inscription$")
    public void estSurLaPageDInscription() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
    }
    @Et("^le champ d'inscription \"nom d'utilisateur\" a été rempli par \"([^\"]*)\"$")
    public void champNomUtilisateurInsc(String nom) throws SQLException {
        driver.findElement(By.name("nomUser")).sendKeys(nom);
    }

    @Et("^la base de donnée est vide$")
    public void viderBDD() throws SQLException {
        db.regenerateBDD();
    }

    @Et("^le champ d'inscription \"mot de passe\" a été rempli par \"([^\"]*)\"$")
    public void champMDP(String mdp) throws SQLException {
        driver.findElement(By.name("mdp")).sendKeys(mdp);
    }

    @Et("^le champ \"prénom\" a été rempli par \"([^\"]*)\"$")
    public void champPrenom(String prenom) throws SQLException {
        driver.findElement(By.name("prenom")).sendKeys(prenom);
    }

    @Et("^le champ \"nom\" a été rempli par \"([^\"]*)\"$")
    public void champNom(String nom) throws SQLException {
        driver.findElement(By.name("nom")).sendKeys(nom);
    }

    @Et("^le champ \"adresse e-mail\" a été rempli par \"([^\"]*)\"$")
    public void champEmail(String email) throws SQLException {
        driver.findElement(By.name("email")).sendKeys(email);
    }

    @Et("^le champ \"confirmation mot de passe\" a été rempli par \"([^\"]*)\"$")
    public void champConfirmMDPInsc(String mdp) throws SQLException {
        driver.findElement(By.name("confirm_mdp")).sendKeys(mdp);
    }

    @Et("^le nom d'utilisateur \"([^\"]*)\" est deja existant dans la base de données$")
    public void utilisateurexistant(String username) throws SQLException {
        if(!db.existUser(username)) {
            db.addUser(username, "Dupont", "Jean", "jean.dupont@test.test", "12345678");
        }
        assertEquals(db.existUser(username), true);
    }

    @Et("^l'email \"([^\"]*)\" est deja existant dans la base de données$")
    public void emailexistant(String mail) throws SQLException {
        if(!db.existEmail(mail)) {
            db.addUser("jedu", "Dupont", "Jean", mail, "12345678");
        }
        assertEquals(db.existEmail(mail), true);
    }

    @Et("^les données entrées sont en attente de validation par l'administrateur pour l'utilisateur \"([^\"]*)\"$")
    public void donneeEnAttente(String username) throws SQLException {
        assertEquals(db.enAttente(username), true);
    }

    @Quand("^l'utilisateur soumet le formulaire$")
    public void envoyerConnexion() throws SQLException {
        driver.findElement(By.className("btn-primary")).click();
    }

    @Alors("^un message d'erreur s'affiche indiquant que l'adresse e-mail est mal formatée$")
    public void messageerreurInsc1() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Le format de l'email est invalide"));
    }

    @Alors("^un message d'erreur s'affiche indiquant que l'adresse e-mail doit être renseigné$")
    public void messageerreurInsc2() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Entrer un email."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le mot de passe doit être renseigné$")
    public void messageerreurInsc3() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Entrer un mot de passe."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le nom d'utilisateur doit être renseigné$")
    public void messageerreurInsc4() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Entrer un nom d'utilisateur"));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le nom doit être renseigné$")
    public void messageerreurInsc5() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Entrer un nom."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le prénom doit être renseigné$")
    public void messageerreurInsc6() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Entrer un prenom."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le mot de passe est trop court$")
    public void messageerreurInsc7() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Le mot de passe doit avoir au moins 6 caractères."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que le nom d'utilisateur est déjà utilisé$")
    public void messageerreurInsc8() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Ce nom d'utilisateur existe déjà."));
    }

    @Alors("^un message d'erreur s'affiche indiquant que l'adresse e-mail est déjà utilisée$")
    public void messageerreurInsc9() throws SQLException {
        assertEquals(baseUrl + "php/register.php", driver.getCurrentUrl());
        assertThat(driver.findElement(By.cssSelector(".has-error > .help-block")).getText(), is("Ce nom d'utilisateur existe déjà."));
    }

    @Alors("^un message de validation s'affiche$")
    public void messagevalidation() throws SQLException {
        assertEquals(baseUrl + "index.php", driver.getCurrentUrl());
    }

}
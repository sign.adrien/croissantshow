DELETE FROM Participation;
DELETE FROM Message;
DELETE FROM MercrediCroissant;
DELETE FROM User WHERE id >0;
INSERT INTO User VALUES ('1', 'loic', 'Loic', 'Grandperrin', 'loic.grandperrin@croissantshow.fr', '$2y$10$RPKK.Qw38DWbS2VL/cYlMO1tkjIE.p4RIuJu24OAiupOqhD6.n57m', '0', '0', '0', '1');
INSERT INTO User VALUES ('2', 'jeremy', 'Jeremy', 'Thiebaud', 'jeremy.thiebaud@croissantshow.fr', '$2y$10$RPKK.Qw38DWbS2VL/cYlMO1tkjIE.p4RIuJu24OAiupOqhD6.n57m', '0', '0', '0', '0');
INSERT INTO User VALUES ('3', 'cynthia', 'Cynthia', 'Maillard', 'cynthia.maillard@croissantshow.fr', '$2y$10$RPKK.Qw38DWbS2VL/cYlMO1tkjIE.p4RIuJu24OAiupOqhD6.n57m', '2', '0', '0', '1');
INSERT INTO User VALUES ('4', 'murat', 'Murat', 'Cosgun', 'murat.cosgun@croissantshow.fr', '$2y$10$RPKK.Qw38DWbS2VL/cYlMO1tkjIE.p4RIuJu24OAiupOqhD6.n57m', '3', '0', '0', '0');
INSERT INTO User VALUES ('5', 'adrien', 'Adrien', 'Signoret', 'adrien.signoret@croissantshow.fr', '$2y$10$RPKK.Qw38DWbS2VL/cYlMO1tkjIE.p4RIuJu24OAiupOqhD6.n57m', '5', '0', '0', '1');

<?php


session_start();
require_once 'php/bibli_generale.php';
$bd = bd_connect();


if(!isset($_SESSION["id"])){
    header("location: php/login.php");
    exit;
}
$sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant)";
$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
$t = mysqli_fetch_assoc($res);
if ($t['date'] < date('Y-m-d')) {
    majMercredi($bd);
}



html_debut("Accueil","php/styles.css");



// Utilisateur validé ou non ?
$sql = "SELECT validInscription FROM User WHERE id = '" . $_SESSION['id'] . "'";
$res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
$t = mysqli_fetch_assoc($res);
$valid = $t['validInscription'];

//utilisateur pas validé -> message
    if ($valid == 0) {
        navbarLock("./php");
        echo '<div class="page-header">',
        '<h1><b><?php', $_SESSION["nomUser"], '</b>Bienvenue sur CROISSANTSHOW. </h1>',
        '<h1> L\'administrateur doit encore valider votre inscription. Veuillez patienter</h1>',
        '</div>';

    } else { //utilisateur validé -> affichage mercredis

        navbar($_SESSION['id']==0, "./php");

        if (isset($_POST['submit'])) {
            $id = $_POST['idMessage'];
            $sql = "DELETE FROM Message WHERE idUser = '" . $_SESSION["id"] . "' AND id =" . $id . ";";
            $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
        }



        echo '<h1>Bienvenue</h1>';
        if($_SESSION['id']>0){
            echo '<h2>Votre prochain Mercredi</h2>';
            $sqlCurrentMerc = 'SELECT * FROM MercrediCroissant INNER JOIN Participation WHERE MercrediCroissant.idMercredi = Participation.idMercredi AND Participation.idUser = "'.$_SESSION['id'].'" LIMIT 1';
            $resCurrentMerc = mysqli_query($bd, $sqlCurrentMerc) or bd_erreur($bd, $sqlCurrentMerc);
            $tCurrentMerc = mysqli_fetch_assoc($resCurrentMerc);

            $sqlNbPart = 'SELECT COUNT(idUser) AS sum FROM Participation WHERE idMercredi = "'.$tCurrentMerc['idMercredi'].'"';
            $resNbPart = mysqli_query($bd, $sqlNbPart) or bd_erreur($bd, $sqlNbPart);
            $tNbPart = mysqli_fetch_assoc($resNbPart);

            echo '<div id="currentMercredi">',
                '<p>participants : ', $tNbPart['sum'], '</p>',
                '<p>date : ', $tCurrentMerc['date'], '</p>';
                if($tCurrentMerc['respCroissant']==null){
                    echo '<p>Pas de responsable croissant encore désigné</p>';
                }else {
                    echo '<p>Responsable pour les croissants : ', getUserName($bd, $tCurrentMerc['respCroissant']), '</p>';
                }

            echo '</div>';
            //création tab avec les notif

            echo '<h2>Notifications : </h2>';

                $tabNotif = array();
                $idNotif = -1;

                $sql = 'SELECT * FROM Message WHERE idUser = "'.$_SESSION['id'].'"';

                $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

                //recup des mercredis pour le user avec l'id dans session

                //si pas de valeur en retour de la requete SQL
                if(mysqli_num_rows($res)==0){
                    echo 'Aucune notifications à afficher';
                } else {
                    echo '<table class="table table-hover">';

                    while ($t = mysqli_fetch_assoc($res)) {
                        $idNotif++;
                        $lastID = $t['id'];
                        $tabNotif[$idNotif] = array('id' => $t['id'],
                            'titre' => $t['titre'],
                            'contenu' => $t['contenu'],
                            'date' => $t['date']
                        );
                    }

                    echo '<thead>',
                        '<tr>',
                            '<th>Titre</th>',
                            '<th>Message</th>',
                            '<th>Date</th>',
                            '<th>Supprimer</th>',
                        '</tr>',
                    '</thead>';
                    foreach ($tabNotif as $notif){
                        echo '<tr>',
                            '<td>',$notif['titre'],'</td>',
                            '<td>',$notif['contenu'],'</td>',
                            '<td>',$notif['date'],'</td>',
                            '<td>',

                            '<form action="index.php" method="post">',
                                '<input type="hidden" name="idMessage" value=', $notif['id'],'>',
                                '<button type="submit" name="submit" value"remove">X</button>',

                            '</form>',

                            '</td>',
                        '</tr>';
                    }


                    echo '</table>';
                    // libération des ressources
                    mysqli_free_result($res);
                }
        }

    }


html_fin();
?>
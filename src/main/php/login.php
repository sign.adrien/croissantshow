<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["id"])){
  header("location: ../index.php");
  exit;
}
 
// Include config file
require_once "bibli_generale.php";
$bd = bd_connect();
// Define variables and initialize with empty values
$nomUser = $mdp = $email = $nom = $prenom = $confirm_mdp = "";
$nomUser_err = $mdp_err = $prenom_err = $nom_err = $email_err = $confirm_mdp_err = "";
$scoreCroissant = 0;
$dateDernierCroissant = NULL;
$Isadmin = 0;
$validInscription = 0;


 
// Processing form data when form is submitted
function checkuser($nomUser_err, $mdp_err, $nomUser, $mdp){
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $bd = bd_connect();
        // Check if nomUser is empty
        if(empty(trim($_POST["nomUser"]))){
            $nomUser_err = "Entrer votre nom de compte";
        } else{
            $nomUser = trim($_POST["nomUser"]);
        }
        
        // Check if mdp is empty
        if(empty(trim($_POST["mdp"]))){
            $mdp_err = "Entrer votre mot de passe.";
        } else{
            $mdp = trim($_POST["mdp"]);
        }
        
       
            
        // Validate credentials
        if(empty($nomUser_err) && empty($mdp_err)){
            // Prepare a select statement   
            $sql = "SELECT id, nomUser, mdp FROM User WHERE nomUser = ?";
            
            if($stmt = mysqli_prepare($bd, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_nomUser);
                //mysqli_stmt_bind_param($stmt, "d", $param_ValidInscription);
                
                // Set parameters
                $param_nomUser = $nomUser;
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Store result
                    mysqli_stmt_store_result($stmt);
                    
                    // Check if nomUser exists, if yes then verify mdp
                    if(mysqli_stmt_num_rows($stmt) == 1){                    
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt, $id, $nomUser, $hashed_mdp );
                        if(mysqli_stmt_fetch($stmt)){
                            if(password_verify($mdp, $hashed_mdp)){
                            ajoutMercredi($bd);
                                // mdp is correct, so start a new session
                                session_start();
                                // Store data in session variables
                                $_SESSION["id"] = $id;
                                $_SESSION["nomUser"] = $nomUser;
                                
                                
                                // Redirect user to welcome page

                                header("location: ../index.php");
                            } else{
                                // Display an error message if mdp is not valid
                                $mdp_err = "Le mot de passe saisi n'est pas valide.";
                            }
                        }
                    } else{
                        // Display an error message if nomUser doesn't exist
                        $nomUser_err = "Le compte n'existe pas.";
                    }
                } else{
                    echo "OOPSS! Il y a eu une erreur!.";
                }
    
                // Close statement
                mysqli_stmt_close($stmt);
            }
    
          
        }
    
        html_debutLogin("Login");
        html_debut_form(htmlspecialchars($_SERVER["PHP_SELF"]));
        form_text_($nomUser,$nomUser_err);
        form_password($mdp_err);
        form_input_login();
        html_fin_form_Login();
        html_fin(); 
    
     
    
    }else{
        html_debutLogin("Login");
        html_debut_form(htmlspecialchars($_SERVER["PHP_SELF"]));
        form_text_($nomUser,$nomUser_err);
        form_password($mdp_err);
        form_input_login();
        html_fin_form_Login();
        html_fin();
    }
}

checkuser($nomUser_err, $mdp_err, $nomUser, $mdp);



?>
 
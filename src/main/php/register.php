<?php
// Include config file
require_once 'bibli_generale.php';
$bd = bd_connect();
if(isset($_SESSION["id"])){
    header("location: ../index.php");
    exit;
}
// Define variables and initialize with empty values
$nomUser = $mdp = $email = $nom = $prenom = $confirm_mdp = "";
$nomUser_err = $mdp_err = $prenom_err = $nom_err = $email_err = $confirm_mdp_err = "";
$scoreCroissant = 0;
$dateDernierCroissant = NULL;
$Isadmin = 0;
$validInscription = 0;
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate nomUser
    if(empty(trim($_POST["nomUser"]))){
        $nomUser_err = "Entrer un nom d'utilisateur";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM User WHERE nomUser = '" . trim($_POST["nomUser"]) . "'";
        $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
            
        // Set parameters
        $param_nomUser = trim($_POST["nomUser"]);
        if(mysqli_num_rows($res)==1){
            $nomUser_err = "Ce nom d'utilisateur existe déjà.";
        } else{
            $nomUser = trim($_POST["nomUser"]);
        }
    }
    
    // Validate mdp
    if(empty(trim($_POST["mdp"]))){
        $mdp_err = "Entrer un mot de passe.";     
    } elseif(strlen(trim($_POST["mdp"])) < 6){
        $mdp_err = "Le mot de passe doit avoir au moins 6 caractères.";
    } else{
        $mdp = trim($_POST["mdp"]);
    }
    
    // Validate confirm mdp
    if(empty(trim($_POST["confirm_mdp"]))){
        $confirm_mdp_err = "Confirmer votre mot de passe.";     
    } else{
        $confirm_mdp = trim($_POST["confirm_mdp"]);
        if(empty($mdp_err) && ($mdp != $confirm_mdp)){
            $confirm_mdp_err = "Les mots de passe ne correspondent pas.";
        }
    }
    if(empty(trim($_POST["nom"]))){
        $nom_err = "Entrer un nom.";
    }
    
    if(empty(trim($_POST["prenom"]))){
        $prenom_err = "Entrer un prenom.";
    }
    if(empty(trim($_POST["email"]))){
        $prenom_err = "Entrer un email.";
    }else{
        if (!filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
            $email_err = "Le format de l'email est invalide";
        }
    }

    
    // Check input errors before inserting in database
    if(empty($nomUser_err) && empty($mdp_err) && empty($confirm_mdp_err) && empty($email_err) && empty($nom_err) && empty($prenom_err) ){
        // Prepare an insert statement
        $sql = "INSERT INTO User (nomUser, prenom, nom, email,mdp , scoreCroissant, dateDernierCroissant, isAdmin, validInscription ) VALUES ('" . $_POST["nomUser"] ." ', '" . $_POST["prenom"] . "', '" .$_POST["nom"]."','" . $_POST["email"]."', '" .password_hash($_POST["mdp"], PASSWORD_DEFAULT)."', '$scoreCroissant', '$dateDernierCroissant',  '$Isadmin', '$validInscription' )";
        mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

            $sql = "SELECT id FROM User WHERE nomUser = '" . $param_nomUser ."'";
            $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
            if(mysqli_num_rows($res)==0){
                header('location: login.php');
            }
            else {
                $t = $res->fetch_assoc();
                session_start();
                $_SESSION['id'] = $t['id'];
                $_SESSION['nomUser'] = $param_nomUser;
                header('location: login.php');
            }

        mysqli_free_result($res);
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="styles.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 0px; }
    </style>
</head>
<body style="background-image: url('../images/BACKGROUND-GRIS-min.jpg');">
    <div class="wrapper">
        <h2>Inscription</h2>
        <p>Veuillez remplir ce formulaire pour créer un compte.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($nomUser_err)) ? 'has-error' : ''; ?>">
                <label>Nom de compte</label>
                <input type="text" name="nomUser" class="form-control" value="<?php echo $nomUser; ?>">
                <span class="help-block"><?php echo $nomUser_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($nom_err)) ? 'has-error' : ''; ?>">
                <label>Nom</label>
                <input type="text" name="nom" class="form-control" value="<?php echo $nom; ?>">
                <span class="help-block"><?php echo $nom_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($prenom_err)) ? 'has-error' : ''; ?>">
                <label>Prenom</label>
                <input type="text" name="prenom" class="form-control" value="<?php echo $prenom; ?>">
                <span class="help-block"><?php echo $prenom_err; ?></span>
            </div> 
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <label>email</label>
                <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div> 

            <div class="form-group <?php echo (!empty($mdp_err)) ? 'has-error' : ''; ?>">
                <label>Mot de passe</label>
                <input type="password" name="mdp" class="form-control" value="<?php echo $mdp; ?>">
                <span class="help-block"><?php echo $mdp_err; ?></span>
            </div>

            <div class="form-group <?php echo (!empty($confirm_mdp_err)) ? 'has-error' : ''; ?>">
                <label>Confirmer le mot de passe</label>
                <input type="password" name="confirm_mdp" class="form-control" value="<?php echo $confirm_mdp; ?>">
                <span class="help-block"><?php echo $confirm_mdp_err; ?></span>
            </div>
            <div class="form-group">
                <input type="Submit" class="btn btn-primary" value="Envoyer">
            </div>
            <p>Deja un compte? <a href="login.php">Connexion</a>.</p>
        </form>
    </div>    
</body>
</html>
<?php


session_start();
require_once 'bibli_generale.php';
$bd = bd_connect();

if(!isset($_SESSION["id"])){
    header("location: php/login.php");
    exit;
}


// Utilisateur validé ou non ?
$sql = "SELECT validInscription FROM User WHERE id = '" . $_SESSION['id'] ."'";
$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
$t = mysqli_fetch_assoc($res);
$valid= $t['validInscription'];

//utilisateur pas validé -> message
if($valid==0){
    header("location: ../index.php");
   exit;
}


html_debut("Accueil");

navbar($_SESSION['id']==0, ".");


    //gestion si suppression de participation à un mercredi
        if (isset($_POST['submit'])) {
            $id = $_POST['idMercredi'];
            $sql = "DELETE FROM Participation WHERE idUser = '" . $_SESSION["id"] . "' AND idMercredi =" . $id . ";";
            $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
            $sql = "SELECT COUNT(idUser) AS nbpart FROM Participation WHERE idMercredi =" . $id . ";";
            $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
            $t2 = mysqli_fetch_assoc($res);
            if($t2['nbpart']<3){
                msgGeneralSuppMercredi3($bd,$id);
                $sql = "DELETE FROM MercrediCroissant WHERE idMercredi = " . $id . ";";
                $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
                $sql = "SELECT idMercredi FROM MercrediCroissant LIMIT 1";
                $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
                $t = mysqli_fetch_assoc($res);
                if($id === $t['idMercredi']){
                    respFinder($bd);
                }
            }
            else {
                $sql = "SELECT * FROM MercrediCroissant WHERE idMercredi = '" . $id . "'";
                $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
                $t = mysqli_fetch_assoc($res);
                if ($t['respCroissant'] == $_SESSION['id']) {
                    $sql = "SELECT id FROM User, Participation, MercrediCroissant WHERE User.id = Participation.idUser AND Participation.idMercredi = MercrediCroissant.idMercredi AND MercrediCroissant.idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant) ORDER BY scoreCroissant, dateDernierCroissant LIMIT 1";
                    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
                    $t4 = mysqli_fetch_assoc($res);
                    $sql = "UPDATE MercrediCroissant SET respCroissant = " . $t4['id'] . " WHERE idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant)";
                    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);

                    //Message changement de responsable (tous participant + nouveau responsable)
                    msgGeneralModifResp($bd, $id, $t4['id']);
                    msgNewResp($bd, $t4['id'], $t['date']);
                }
            }
        }



    echo '<h1>Liste des mercredis</h1>';


    //création tab avec les mercredi

    $currentMerc = array();
    $idMercr = -1;

    $sql = 'SELECT * FROM MercrediCroissant INNER JOIN Participation WHERE MercrediCroissant.idMercredi = Participation.idMercredi AND Participation.idUser = "'.$_SESSION['id'].'"';

    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

    //recup des mercredis pour le user avec l'id dans session

    //si pas de valeur en retour de la requete SQL
    if(mysqli_num_rows($res)==0){
        echo 'Vous ne participez à aucun mercredi';
    } else {
        echo '<table class="table table-hover">';

        while ($t = mysqli_fetch_assoc($res)) {
            $idMercr++;
            $lastID = $t['idMercredi'];
            $currentMerc[$idMercr] = array('id' => $t['idMercredi'],
                'date' => $t['date']
            );
        }

        echo '<thead>',
            '<tr>',
                '<th>Date</th>',
                '<th>Ne plus participer</th>',
            '</tr>',
        '</thead>';
        foreach ($currentMerc as $mercredi){
            echo '<tr>',
                '<td>',$mercredi['date'],'</td>',
                '<td>',

                '<form action="listMercredi.php" method="post">',
                    '<input type="hidden" name="idMercredi" value=', $mercredi['id'],'>',
                    '<button type="submit" name="submit" value"remove">X</button>',

                '</form>',

                '</td>',
            '</tr>';
        }


        echo '</table>';

        // libération des ressources
        mysqli_free_result($res);

    }



html_fin();

?>
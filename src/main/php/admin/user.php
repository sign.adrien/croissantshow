<?php
// Initialize the session
session_start();
 

if(!isset($_SESSION["id"]) || $_SESSION["id"] !== 0){
  header("location: ../index.php");
  exit;
}

// Include config file
require_once "../bibli_generale.php";




$bd = bd_connect();




//gestion si suppression de participation à un mercredi
if(isset($_POST['submitAdd'])){
  $id = $_POST['submitAdd'];
  $sql = "UPDATE User SET validInscription = 1 WHERE id ='" . $id ."';";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  $sql = "SELECT idMercredi FROM MercrediCroissant";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  if(mysqli_num_rows($res)!=0) {
    $t = mysqli_fetch_all($res);
    $sql = "INSERT INTO Participation (idUser, idMercredi) VALUES ";
    $size = count($t);
    for($i = 0; $i< $size-1;$i++) {
      $sql = $sql . "(" . $id . "," . $t[$i][0] . "),";
    }
    $sql = $sql."(".$id.",".$t[$size-1][0].");";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  }
  $newRespID = respFinder($bd);
  //Message pour prévenir le responsable
  if($newRespID == $id) {
    $sql = "SELECT idMercredi FROM MercrediCroissant ORDER BY idMercredi LIMIT 1";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
    $t = mysqli_fetch_assoc($res);
    $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '" . $t['idMercredi'] . "'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    msgNewResp($bd, $newRespID, $tMsg['date']);
  }
}
if(isset($_POST['submitRemove'])){
  $id = $_POST['submitRemove'];

  $sql = "UPDATE User SET validInscription = 0 WHERE id ='" . $id ."';";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  $sql = "DELETE FROM Participation WHERE idUser = '".$id."'";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  checkMercredisParticipations($bd);
  $newRespID = respFinder($bd);
  if($newRespID == $id) {
    $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '" . $t['idMercredi'] . "'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    msgNewResp($bd, $newRespID, $tMsg['date']);
  }
}

if(isset($_POST['submitDelete'])){
  $id = $_POST['submitDelete'];
  $sql = "DELETE FROM Participation WHERE idUser = '".$id."'";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  $sql = "DELETE FROM User WHERE id = '".$id."'";
  $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
  checkMercredisParticipations($bd);
  $newRespID = respFinder($bd);
  if($newRespID == $id) {
    $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '" . $t['idMercredi'] . "'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    msgNewResp($bd, $newRespID, $tMsg['date']);
  }
}



html_debut("Gestion utilisateur","../styles.css");
navbar($_SESSION['id']==0, "..");
html_user_body();



$sql = 'SELECT * FROM User WHERE id != 1';

$res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);

//si pas de valeur en retour de la requete SQL
if(mysqli_num_rows($res)==0){
  echo 'Aucun utilisateur à afficher';
} else {
    while ($t = mysqli_fetch_assoc($res)) {
      display_line_user($t['id'], $t['nomUser'],$t['nom'], $t['prenom'], $t['email'], $t['dateDernierCroissant'], $t['validInscription']);
    }
}
  html_fin_user();

?>


<?php
require_once '../bibli_generale.php';
require_once '../GoogleAPI/settings.php';
$bd = bd_connect();
session_start();
//verif admin en fonction de l'id de la var de session
if (!isset($_SESSION["id"]) || $_SESSION["id"] !== 0) {
    header("location: ../welcome.php");
    exit;
}

//gestion si suppression de participation à un mercredi
if (isset($_POST['submit'])) {
    $id = $_POST['idMercredi'];
    $sql = "SELECT idMercredi FROM MercrediCroissant ORDER BY idMercredi ASC LIMIT 1";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $t = mysqli_fetch_assoc($res);
    $sql = "DELETE FROM MercrediCroissant WHERE idMercredi =" . $id . ";";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    if ($id === $t['idMercredi']) {
        respFinder($bd);
    }
}

if (isset($_POST['GOOGLE'])) {
    

}


html_debut("Admin - Gestion Mercredi","../styles.css");

navbar($_SESSION['id']==0, "..");


echo '<h1>Gestion des mercredis</h1>';

//création tab avec les mercredi


$currentMerc = array();
$idMercr = -1;


$sql = 'SELECT * FROM MercrediCroissant';

$res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);

//recup des mercredis pour le user avec l'id dans session

//si pas de valeur en retour de la requete SQL
if (mysqli_num_rows($res) == 0) {
    echo 'Aucun mercredi à afficher';
} else {
    echo '<table class="table table-hover">';

    while ($t = mysqli_fetch_assoc($res)) {
        $idMercr++;
        $lastID = $t['idMercredi'];
        $currentMerc[$idMercr] = array('id' => $t['idMercredi'],
            'date' => $t['date'],
            'resp' => $t['respCroissant']
        );
    }

    echo '<thead>',
    '<tr>',
    '<th>Date</th>',
    '<th>Responsable</th>',
    '<th>Supprimer le Mercredi</th>',
    '</tr>',
    '</thead>';
    foreach ($currentMerc as $mercredi) {
        echo '<tr>',
        '<td>', $mercredi['date'], '</td>';
        if($mercredi['resp']==null){
            echo '<td></td>';
        }else{
            echo '<td>', getUserName($bd, $mercredi['resp']), '</td>';
        }

        echo '<td>',

        '<form action="gestionMercredi.php" method="post">',
        '<input type="hidden" name="idMercredi" value=', $mercredi['id'], '>',
        '<button type="submit" name="submit" value"remove">X</button>',
        '<button type="submit" name="GOOGLE" value"GOOGLE">GOOGLE</button>',

        '</form>',

        '</td>',
        '</tr>';
    }


    echo '</table>';

    // libération des ressources
    mysqli_free_result($res);

}


html_fin();

?>

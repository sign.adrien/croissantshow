<?php

require_once 'sqlconfig.php';

//---------------------------------------------------------------
// Définition des types de zones de saisies
//---------------------------------------------------------------
define('bg_Z_TEXT', 'text');
define('bg_Z_PASSWORD', 'password');
define('bg_Z_NUMBER', 'number');
define('bg_Z_CHECK', 'checkbox');
define('bg_Z_SUBMIT', 'submit');
define('bg_Z_HIDDEN', 'hidden');


/**
 *	Fonction affichant le début du code HTML d'une page.
 *
 *  @param 	String	$titre	Titre de la page
 *	@param 	String	$css	Chemin relatif vers la feuille de style CSS.
 */
	function html_debut($title,$style = "styles.css") {
	echo '<!doctype html>',
			'<html lang="fr">',
				'<head>',
					'<meta name="viewport" content="width=device-width" />',
					'<meta charset="UTF-8">',
                    '<title>',$title,'</title>',
                    '<link rel="stylesheet" href="',$style,'" />',
                    '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">',
                    '<meta name="viewport" content="width=device-width, initial-scale=1">',
                    '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">',
                    '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>',
                    '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>';
                   
					$nbArg = func_num_args();
  					for ($i = 1; $i < $nbArg; $i++) {
						echo '<link type="text/css" media="screen" rel="stylesheet" href="', func_get_arg($i),'">';
					}
				echo '</head>',
                '<body>';
                
    }

    function html_user_body(){
        echo  '<div class="container">',
                '<h1>Tableau utilisateurs</h1>',
                '<table class="table">',
                    '<thead>',
                    '<tr>',
                        '<th>Nom utilisateur</th>',
                        '<th>Nom</th>',
                        '<th>Prénom</th>',
                        '<th>Mail</th>',
                        '<th>Date Dernier Croissant</th>',
                        '<th>Valider l\'inscription</th>',
                    '</tr>',
                    '</thead>',
                    '<tbody>';
    }
    function html_fin_user(){
        echo '</tbody>',
        '</table>',
      '</div>',
    '</body>',
  '</html>';
    }

    function display_line_user($id ,$nomUser, $nom, $prenom, $mail, $dateDernierCroissant, $validInscription){
        echo '<tr>',
            '<td>',$nomUser,'</td>',
            '<td>',$nom,'</td>',
            '<td>',$prenom,'</td>',
            '<td>',$mail,'</td>',
            '<td>',$dateDernierCroissant,'</td>',
            '<td>';

            if($validInscription == 0 ){
                echo '<form action="user.php" method="post">',
                    '<input type="hidden" id="', $id, '" name="idUser" value=', $id,'>',
                    '<button type="submit" name="submitAdd" id="btn-ins-',$nomUser,'" value="',$id,'">Inscrire</button>',
                    '</td>';
            }else{
                echo '<form action="user.php" method="post">',
                '<input type="hidden" id="', $id, '" name="idUser" value=', $id,'>',
                '<button type="submit" name="submitRemove" id="btn-ins-',$nomUser,'" value="',$id,'">✓</button>',
                '</td>';
            }

            echo ' <td>
            <form action="user.php" method="post">',
                    '<input type="hidden" id="', $id, '" name="idUser" value=', $id,'>',
                    '<button type="submit" name="submitDelete" id="btn-ins-',$nomUser,'" value="',$id,'">Supprimer</button>',
                    '</td>';

          '</tr>';
    }

    function html_debutLogin($title) {
        echo '<!doctype html>',
			'<html lang="fr">',
				'<head>', '<meta charset="UTF-8">',
                '<title>Login</title>',
            '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">',
            '<link rel="stylesheet" href="styles.css">',
            '<style type="text/css">',
                'body{ font: 14px sans-serif; }',
                '.wrapper{ width: 350px; padding: 20px; }',
            '</style>',
        '</head>',
        '<body>',
            '<div class="wrapper">',
            '<h2>Connexion</h2>',
            '<p>Veuilez remplir les champs pour vous connecter.</p>';
        }
    function html_debut_form($action){
        echo '<form action=',$action,' method="post">';
    }

    function html_fin_form_Login(){
        echo '<p>Pas de compte? <a href="register.php">Inscrit toi!</a></p>',
        '</form>',
        '</div>';
    }


/**
 *	Fonction affichant la fin du code HTML d'une page.
 */
function html_fin() {
	echo '</body></html>';
}

//-------------------------------------------------

//NAVBAR

function navbar($isAdmin, $url){
    echo '<nav class="navbar navbar-default">',
      '<div class="container-fluid">',

        '<div class="navbar-header">',
          '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">',
            '<span class="sr-only">Toggle navigation</span>',
            '<span class="icon-bar"></span>',
            '<span class="icon-bar"></span>',
            '<span class="icon-bar"></span>',
          '</button>',
          '<a class="navbar-brand" href="',$url,'/../index.php">CroissantShow</a>',
       ' </div>';

        if($isAdmin){
            echo '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">',
                   '<ul class="nav navbar-nav">',
                        '<li><a href="',$url,'/../index.php">Accueil<span class="sr-only"></a></li>',
                        '<li><a href="',$url,'/admin/user.php">Gestion Users<span class="sr-only"></a></li>',
                        '<li><a href="',$url,'/admin/gestionMercredi.php">Gestion Mercredis</a></li>',
                    '</ul>',
                       '<ul class="nav navbar-nav navbar-right">',
                                           '<li><a href="',$url,'/logout.php" id="btn-deconnexion">Deconnexion</a></li>',
                                      '</ul>',
                 '</div>';
        }else{
            echo '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">',
               '<ul class="nav navbar-nav">',
                    '<li><a href="',$url,'/../index.php">Accueil<span class="sr-only"></a></li>',
                    '<li><a href="',$url,'/listMercredi.php">Mes Mercredis<span class="sr-only"></a></li>',
               '</ul>',
               '<ul class="nav navbar-nav navbar-right">',
                    '<li><a href="',$url,'/logout.php" id="btn-deconnexion">Deconnexion</a></li>',
               '</ul>',
             '</div>';
        }


      echo '</div>',
        '</nav>';
}

function navbarLock($url){
    echo '<nav class="navbar navbar-default">',
      '<div class="container-fluid">',

        '<div class="navbar-header">',
          '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">',
            '<span class="sr-only">Toggle navigation</span>',
            '<span class="icon-bar"></span>',
            '<span class="icon-bar"></span>',
            '<span class="icon-bar"></span>',
          '</button>',
          '<a class="navbar-brand" href="',$url,'/../index.php">CroissantShow</a>',
       ' </div>';


            echo '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">',
               '<ul class="nav navbar-nav">',
               '</ul>',
               '<ul class="nav navbar-nav navbar-right">',
                    '<li><a href="',$url,'/logout.php" id="btn-deconnexion">Deconnexion</a></li>',
               '</ul>',
             '</div>';


      echo '</div>',
        '</nav>';
}


//____________________________________________________________________________
/**
 *	Ouverture de la connexion à la base de données
 *
 *	@return objet 	connecteur à la base de données
 */
function bd_connect() {
    $conn = mysqli_connect(BS_SERVER, BS_USER, BS_PASS, BS_DB);
    if ($conn !== FALSE) {
        //mysqli_set_charset() définit le jeu de caractères par défaut à utiliser lors de l'envoi
        //de données depuis et vers le serveur de base de données.
        mysqli_set_charset($conn, 'utf8')
        or bg_bd_erreurExit('<h4>Erreur lors du chargement du jeu de caractères utf8</h4>');
        return $conn;     // ===> Sortie connexion OK
    }
    // Erreur de connexion
    // Collecte des informations facilitant le debugage
    $msg = '<h4>Erreur de connexion base MySQL</h4>'
            .'<div style="margin: 20px auto; width: 350px;">'
            .'BD_SERVER : '. BS_SERVER
            .'<br>BS_USER : '. BS_USER
            .'<br>BS_PASS : '. BS_PASS
            .'<br>BS_DB : '. BS_DB
            .'<p>Erreur MySQL numéro : '.mysqli_connect_errno()
            .'<br>'.htmlentities(mysqli_connect_error(), ENT_QUOTES, 'ISO-8859-1')
            //appel de htmlentities() pour que les éventuels accents s'affiche correctement
            .'</div>';
    bd_erreurExit($msg);
}

//____________________________________________________________________________
/**
 * Arrêt du script si erreur base de données
 *
 * Affichage d'un message d'erreur, puis arrêt du script
 * Fonction appelée quand une erreur 'base de données' se produit :
 * 		- lors de la phase de connexion au serveur MySQL
 *		- ou indirectement lorsque l'envoi d'une requête échoue
 *
 * @param string	$msg	Message d'erreur à afficher
 */
function bd_erreurExit($msg) {
    ob_end_clean();	// Supression de tout ce qui a pu être déja généré
    ob_start('ob_gzhandler'); // nécessaire sur saturnin quand compression avec ob_gzhandler
    echo    '<!DOCTYPE html><html lang="fr"><head><meta charset="UTF-8"><title>',
            'Erreur base de données</title>',
            '<style>table{border-collapse: collapse;}td{border: 1px solid black;padding: 4px 10px;}</style>',
            '</head><body>',
            $msg,
            '</body></html>';
    exit(1);
}


//____________________________________________________________________________
/**
 * Gestion d'une erreur de requête à la base de données.
 *
 * A appeler impérativement quand un appel de mysqli_query() échoue
 * Appelle la fonction bg_bd_erreurExit() qui affiche un message d'erreur puis termine le script
 *
 * @param objet		$bd		Connecteur sur la bd ouverte
 * @param string	$sql	requête SQL provoquant l'erreur
 */
function bd_erreur($bd, $sql) {
    $errNum = mysqli_errno($bd);
    $errTxt = mysqli_error($bd);

    // Collecte des informations facilitant le debugage
    $msg =  '<h4>Erreur de requête</h4>'
            ."<pre><b>Erreur mysql :</b> $errNum"
            ."<br> $errTxt"
            ."<br><br><b>Requête :</b><br> $sql"
            .'<br><br><b>Pile des appels de fonction</b></pre>';

    // Récupération de la pile des appels de fonction
    $msg .= '<table>'
            .'<tr><td>Fonction</td><td>Appelée ligne</td>'
            .'<td>Fichier</td></tr>';

    $appels = debug_backtrace();
    for ($i = 0, $iMax = count($appels); $i < $iMax; $i++) {
        $msg .= '<tr style="text-align: center;"><td>'
                .$appels[$i]['function'].'</td><td>'
                .$appels[$i]['line'].'</td><td>'
                .$appels[$i]['file'].'</td></tr>';
    }

    $msg .= '</table>';

    bd_erreurExit($msg);	// => ARRET DU SCRIPT
}

function protect_sortie($str) {
	$str = trim($str);
	return htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function bd_protect($bd, $str) {
	$str = trim($str);
	return mysqli_real_escape_string($bd, $str);
}

function bd_protect_plus($bd, $str) {
    $str = trim($str);
    return mysqli_real_escape_string($bd, htmlentities($str, ENT_QUOTES, 'UTF-8'));
}

function redirige($page) {
	header("Location: $page");
	exit();
}

function exit_session() {
	session_destroy();
	session_unset();
	$cookieParams = session_get_cookie_params();
	setcookie(session_name(),
			'',
			time() - 86400,
         	$cookieParams['path'],
         	$cookieParams['domain'],
         	$cookieParams['secure'],
         	$cookieParams['httponly']
    	);

	header('Location: ../index.php');
	exit();
}

function est_entier($x) {
    return is_numeric($x) && ($x == (int) $x);
}

function form_ligne($gauche, $droite) {
    $gauche =  protect_sortie($gauche);
    return "<tr><td>{$gauche}</td><td>{$droite}</td></tr>";
}

function form_input($type, $name, $value, $size=0) {
   $value =  protect_sortie($value);
   $size = ($size == 0) ? '' : "size='{$size}'";
   return "<input type='{$type}' id='{$name}' name='{$name}' {$size} value='{$value}' requiered>";
}
function form_text_($nomUser,$nomUser_err){
    echo '<div class="form-group';
            if (!empty($nomUser_err)){
                echo 'has-error';
            }else{
                echo '';
            }
             echo '">';
                echo '<label>Nom de Compte</label>',
                '<input type="text" name="nomUser" class="form-control" value="',$nomUser,'">',
                '<span class="help-block" style="color : red;">',$nomUser_err,'</span>',
            '</div>';
}
function form_password($nomUser_err){
    echo '<div class="form-group'; echo (!empty($nomUser_err)) ? 'has-error' : ''; echo '">';
                echo '<label>Mot de passe</label>',
                ' <input type="password" name="mdp" class="form-control">',
                '<span class="help-block" style="color : red;">',$nomUser_err,'</span>',
            '</div>';
}

function form_input_login(){
    echo '<div class="form-group">',
        '<input type="submit" class="btn btn-primary" id="btnConnect" value="Login">',
        '</div>';

}

/* ------------------------------------------------------------ */

//Nom utilisateur a partir de l'id
function getUserName($bd, $id){
    $sql = "SELECT nomUser FROM User WHERE id = '" . $id . "'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
    $t = mysqli_fetch_assoc($res);
    return $t['nomUser'];
}


//mise a jour mercredi et score mercredi
function majMercredi($bd){
    $sql = "SELECT idMercredi, date, respCroissant FROM MercrediCroissant WHERE idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant)";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
    $t = mysqli_fetch_assoc($res);
         $sql = "SELECT COUNT(idUser) AS nb FROM Participation WHERE idMercredi = '" . $t['idMercredi'] . "'";
         $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
         $t2 = mysqli_fetch_assoc($res);
         $sql = "SELECT scoreCroissant FROM User WHERE id = '" . $t['respCroissant'] . "'";
         $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
         $t3 = mysqli_fetch_assoc($res);
         $sql = "UPDATE User SET scoreCroissant = '" . ($t3['scoreCroissant'] + $t2['nb']) . "', dateDernierCroissant = '" . $t['date'] . "' WHERE id = '" . $t['respCroissant'] . "';";
         $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
         $sql = "DELETE FROM MercrediCroissant WHERE idMercredi = '" . $t['idMercredi'] . "'";
         $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
         $newRespID = respFinder($bd);
         //Message pour prévenir le responsable
         $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '".$t['idMercredi']."'";
         $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
         $tMsg = mysqli_fetch_assoc($res);
         msgNewResp($bd, $newRespID, $tMsg['date']);
}

function  respFinder($bd){
    $sql = "SELECT date FROM MercrediCroissant ORDER BY idMercredi LIMIT 1";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $t = mysqli_fetch_assoc($res);
    $mod_date = strtotime($t['date']."- 7 days");
    $t["date"] = date("Y-m-d",$mod_date);
    $sql = "SELECT id FROM User, Participation, MercrediCroissant WHERE dateDernierCroissant != " .$t['date'] . " AND User.id = Participation.idUser AND Participation.idMercredi = MercrediCroissant.idMercredi AND MercrediCroissant.idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant) ORDER BY scoreCroissant LIMIT 1";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $t2 = mysqli_fetch_assoc($res);
    $sql = "UPDATE MercrediCroissant SET respCroissant = " . $t2['id'] . " WHERE idMercredi = (SELECT MIN(idMercredi) FROM MercrediCroissant)";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    return $t2['id'];
}

function checkMercredisParticipations($bd){
    $sql = "SELECT COUNT(idUser) AS nb, idMercredi From Participation GROUP BY idMercredi";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
    $t =mysqli_fetch_assoc($res);
    for($i = 0; $i< count($t);$i++){
        if($t['nb']<3){
            msgGeneralSuppMercredi3($bd,$id);
            $sql = "DELETE FROM MercrediCroissant WHERE idMercredi = " . $t['idMercredi'] . ";";
            $res =  mysqli_query($bd, $sql) or bd_erreur($bd,$sql);
        }
    }
}

//GESTION DES MESSAGES

function msgGeneralModifResp($bd, $idMercredi, $idResp){

    //recup date du mercredi
    $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '".$idMercredi."'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    $dateMercredi = $tMsg['date'];

    $sql = "SELECT * FROM Participation WHERE idMercredi = '".$idMercredi."'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);


    while($t = mysqli_fetch_assoc($res)) {
        $titre = "Nouveau responsable !";
        $msg = "Le nouveau responsable croissant du mercredi ".$dateMercredi." est ". getUserName($bd, $idResp);
        $date = date('Y-m-d');
        $sqlMsg = "INSERT INTO Message (titre, contenu, date, idUser) VALUES ('" . $titre ." ', '" . $msg ." ', '" . $date ." ', " . $t['idUser'] ." )";
        $resMsg = mysqli_query($bd, $sqlMsg) or bd_erreur($bd, $sqlMsg);
    }


}

function msgGeneralSuppMercredi3($bd, $idMercredi){

    //recup date du mercredi
    $sql = "SELECT date FROM MercrediCroissant WHERE idMercredi = '".$idMercredi."'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    $dateMercredi = $tMsg['date'];

    $sql = "SELECT * FROM Participation WHERE idMercredi = '".$idMercredi."'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);


    while($t = mysqli_fetch_assoc($res)) {
        $titre = "Pas assez de participants";
        $msg = "Le Mercredi ".$dateMercredi." a été annulé en raison du nombre insuffisant de participant.";
        $date = date('Y-m-d');
        $sqlMsg = "INSERT INTO Message (titre, contenu, date, idUser) VALUES ('" . $titre ." ', '" . $msg ." ', '" . $date ." ', " . $t['idUser'] ." )";
        $resMsg = mysqli_query($bd, $sqlMsg) or bd_erreur($bd, $sqlMsg);
    }


}

function msgNewResp($bd, $idResp, $dateMerc){
    $titre = "Changement de Responsable";
    $msg = "Vous êtes chargé de ramener les croissants pour le mercredi ".$dateMerc;
    $date = date('Y-m-d');
    $sql = "INSERT INTO Message (titre, contenu, date, idUser) VALUES ('" . $titre ." ', '" . $msg ." ', '" . $date ." ', " . $idResp ." )";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
}

function ajoutMercredi($bd){
    $mois = date("m");
    $annee = date("Y");
    $nextAnnee = $annee +1;
    $today = date("Y-m-d");
    $today2 = date("j F Y");
    $sql = "SELECT COUNT(*) AS Nb FROM MercrediCroissant WHERE date > '".$today."'";
    $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
    $tMsg = mysqli_fetch_assoc($res);
    $NbMercredi = $tMsg['Nb'];
    $lastMercredi = "31 August ";
    if($NbMercredi == 0){
        if($mois > 9){
            $lastMercredi .= $nextAnnee;
        }else{
            $lastMercredi .= $annee;
        }
        $endDate = strtotime($lastMercredi);
        for($i = strtotime('Wednesday', strtotime($today2)); $i <= $endDate; $i = strtotime('+1 week', $i)){
            $sql = "INSERT INTO MercrediCroissant (respCroissant,date) VALUES (NULL,'". date('Y-m-d', $i) . "');";
            $res = mysqli_query($bd, $sql) or bd_erreur($bd, $sql);
        }
    }
}





?>
